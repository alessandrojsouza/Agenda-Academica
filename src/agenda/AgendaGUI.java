/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import agenda.entidades.Pessoa;
import agenda.entidades.PessoaFisica;
import agenda.entidades.PessoaJuridica;
import agenda.excecoes.AgendaException;
import agenda.servicos.AgendaMemoria;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Laut
 */
public class AgendaGUI {

    
    /**
     * @param args the command line arguments
     *
     */
    public static void main(String[] args) {
        AgendaMemoria agenda = new AgendaMemoria();
        PessoaFisica pf = null;
        PessoaJuridica pj = null;
        Scanner sc = new Scanner(System.in);
        int op = 0;
        //While menu
        while (op!=6) {
            printMenu();
            op = escolhaOpcao();
            if (op==1){
                pf = new PessoaFisica();
                
                System.out.println("CPF: ");
                pf.setCpf(sc.next());
                System.out.println("Nome: ");
                pf.setNome(sc.next());
                System.out.println("Sobrenome: ");
                pf.setSobreNome(sc.next());
                System.out.println("Idade: ");
                pf.setIdade(sc.nextInt());
                agenda.gravar(pf);
            }
           
            if (op==2){
                pj = new PessoaJuridica();  
                System.out.println("CNPJ: ");
                pj.setCnpj(sc.next());
                System.out.println("Nome: ");
                pj.setNome(sc.next());
                System.out.println("Sobrenome: ");
                pj.setSobreNome(sc.next());
                System.out.println("Idade: ");
                pj.setIdade(sc.nextInt());
                agenda.gravar(pj);
            }
            
            if (op==3){
                System.out.println("Digite o nome: ");
                String nome = sc.next();
                Pessoa p = null;
                try {
                    p = agenda.getByNome(nome);
                    
                } catch (AgendaException ex) {
                    Logger.getLogger(AgendaGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                agenda.remover(p);
                System.out.println("######## Contato excluído ######## ");
            } 
            
            if (op==4){
                System.out.println("Digite o nome: ");
                String nome = sc.next();
                System.out.println("######## Resultado da Pesquisa ######## ");
                try{
                System.out.println("Contato:" + agenda.getByNome(nome).getNome() +" " +
                        agenda.getByNome(nome).getSobreNome());
                }catch(AgendaException aex){
                    System.err.println(aex.getMessage());
                }
            }                    
            
            if (op==5){
                System.out.println("###### Listagem de todos os contatos #######");
                try {
                    for (Pessoa contato : agenda.listaTodos()) {
                        System.out.println("Contato: " + contato.getNome()+ " " + contato.getSobreNome());
                    }
                } catch (AgendaException aex) {
                    System.err.println(aex.getMessage());
                }
            }
                    
        }

    }

    private static void printMenu() {
        System.out.println("");
        System.out.println("AGENDA DE CONTATOS");
        System.out.println("--------------------------------------");
        System.out.println("1-Adicionar Pessoa Física");
        System.out.println("2-Adicionar Pessoa Jurídica");
        System.out.println("3-Remover contato.");
        System.out.println("4-Lista por nome");
        System.out.println("5-Lista todos");
        System.out.println("6-Sair");
        System.out.println("");
        System.out.println("Digite a opção: ");
    }

    private static int escolhaOpcao() {
        Scanner sc = new Scanner(System.in);
        int opcao =  sc.nextInt();
        return opcao;
    }
}
