/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.servicos;

import agenda.entidades.Pessoa;
import agenda.excecoes.AgendaException;
import java.util.List;

/**
 *
 * @author Laut
 * s
 */
public interface IAgenda {
    public void gravar(Pessoa p);
    public void remover (Pessoa p);
    public Pessoa getByNome(String nome) throws AgendaException;
    public List<Pessoa> listaTodos()throws AgendaException;
     
            
}
