/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.servicos;

import agenda.entidades.Pessoa;
import agenda.excecoes.AgendaException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Laut
 */
public class AgendaMemoria implements IAgenda{

    private List<Pessoa> listaDeContatos = new ArrayList<>();
    
    @Override
    public void gravar(Pessoa p) {
        this.listaDeContatos.add(p);
    }

    @Override
    public void remover(Pessoa p) {
        this.listaDeContatos.remove(p);
    }

    /**
     *
     * @param nome
     * @return
     * @throws AgendaException
     */
    @Override
    public Pessoa getByNome(String nome) throws AgendaException {
        AgendaException ex = new AgendaException("Usuário não encontrado.");
        for (Pessoa Contato : listaDeContatos) {
            if (Contato.getNome().equals(nome))
                return Contato;
        }
        throw ex;
        //return null; trocar por uma exceção
    }

    /**
     *
     * @return
     */
    @Override
    public List<Pessoa> listaTodos() throws AgendaException{
        if(listaDeContatos.isEmpty())
            throw new AgendaException("Não há contatos cadastrados  na agenda.");
        return this.listaDeContatos;
    }
    
}
